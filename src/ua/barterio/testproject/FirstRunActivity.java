package ua.barterio.testproject;

import ua.barterio.testproject.utils.AsyncGetter;
import ua.barterio.testproject.utils.Error;
import ua.barterio.testproject.utils.JsonParser;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class FirstRunActivity extends Activity {
	private static final String URL = "http://json-generator.appspot.com/api/json/get/cbHNXtQQeq?indent=2";
	private static final String TAG = "MainActivity";
	/*
	 * "Необходимо зайти на
	 * http://json-generator.appspot.com/api/json/get/cbHNXtQQeq?indent=2
	 * скачать, распарсить, сохранить в SQLLite, отобразить список верхнего
	 * уровня (интересуют только поля guid, company, email, address, phone,
	 * balance) в таблице и добавить возможность просмотра друзей (по тапу на
	 * ячейку в таблице - переход на список друзей, там выводить только имя
	 * друга). Стоит обратить внимание на: • структуру приложения/распределение
	 * обязанностей между классами • code style/comments •s приложение не должно
	 * зависать или тормозить при загрузке, парсинге и отображении/скролле по
	 * таблицам (hint: no heavy operations on the main thread). • желательно
	 * заюзать какой-то аналог CoreData на Android для доступа к persistence
	 * layer"
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new AsyncGetter() {
			@Override
			public void onPostExecute(String result) {
				if (!result.equals(Error.DATA_ERROR)) {
					new JsonParser().getMainObjectsList(result);
				}
			}
		}.execute(URL);
	}
}
