package ua.barterio.testproject.objects;

public class Friends {
	public static final String TAG = "friends";
	
	public static final String ID = "id";
	public static final String NAME = "name";
	
	private int mId;
	private String mName;
	
	public Friends(int id, String name){
		mId = id;
		mName = name;
	}

	public int getId() {
		return mId;
	}

	public void setId(int id) {
		mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	@Override
	public String toString() {
		return "Friends [mId=" + mId + ", mName=" + mName + "]";
	}
	

}
